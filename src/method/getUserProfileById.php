<?php

namespace postServer\method;

class getUserProfileById extends \postServer\abstraction\method {
    protected $returnFields = array(
        'id'=>'userId',
        'name'=>'name',
        'email'=>'email',
        'gender'=>'gender',
        'birthday'=>'birthday',
        'about'=>'about',
        'image1'=>'image1',
        'image2'=>'image2',
        'image3'=>'image3',
        'image4'=>'image4',
        'image5'=>'image5',
        'image6'=>'image6',
        'isPushNotification'=>'isPushNotification',        
    );
    
    protected $needFields = array(
        "userId"
    );
    
    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
       
        return $this->prepareResult($user->as_array());
        
    }
}
