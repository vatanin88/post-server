<?php

namespace postServer\method;

class historyGamesByUserId extends \postServer\abstraction\method {
    protected $returnFields = array(
        'gameId'=>'gameId',
        'myQuestion'=>'myQuestion',       
    );
    
    protected $needFields = array(
        "userId"
    );
    
    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        $games = \ORM::for_table($this->gameTable)
                ->table_alias('game')
                ->select('game.id','gameId')
                ->select('question.question','myQuestion')
                ->inner_join($this->questionTable, array('game.id', '=', 'question.game'), 'question')
                ->where('question.user', $user->id)
                ->find_many();
        
        $res = array();
        
        foreach ($games as $game) {
            $res[] = $game->as_array();
        }
        
       
        return $this->prepareResult($res, 'array');
        
    }
}
