<?php

namespace postServer\method;

class historyGame extends \postServer\abstraction\method {
    protected $returnFields = array(
        'gameId'=>'gameId',
        'myQuestion'=>'myQuestion',       
    );
    
    protected $needFields = array(
        "gameId"
    );
    
    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $game = \ORM::for_table($this->gameTable)->where('id', $_REQUEST['gameId'])->find_one();
        if(!$game) {
            $this->error[] = 'Game not found';
            return $this->error();
        }
        
   
        
        $users = \ORM::for_table($this->userTable)
                 ->raw_query("SELECT 
                    user.id as userId, 
                    user.name as name , 
                    user.gender as gender, 
                    user.birthday as birthday,
                    user.image1 as image1,
                    user2.id as userId2,
                    user2.name as name2,
                    user2.gender as gender2,
                    user2.birthday as birthday2,
                    user2.image1 as image12
                    FROM {$this->userTable} as user

                    LEFT  JOIN {$this->questionTable} as question ON user.id = question.user
                    LEFT  JOIN `{$this->likeTable}` as `like` ON user.id =  `like`.user
                    LEFT  JOIN `{$this->userTable}` as user2 ON user2.id =  `like`.like_user
                    LEFT  JOIN `{$this->likeTable}` as `like2` ON user2.id =  `like2`.user

                    WHERE question.game = {$game->id} AND `like2`.like_user = user.id
                    GROUP BY user.id
                    ")
                ->find_many();
        
        $mathes = array();
        

        
        foreach ($users as $user) {
            $mathes[] = array(
                "user1" => array(
                    "userId" => $user->userId,
                    "name" => $user->name,
                    "gender" => $user->gender,
                    "birthday" => $user->birthday,
                    "image1" => 'http://' . $_SERVER['HTTP_HOST'] . '/' . dirname($_SERVER['PHP_SELF']) .$user->image1,
                ),
                "user2" => array(
                    "userId" => $user->userId2,
                    "name" => $user->name2,
                    "gender" => $user->gender2,
                    "birthday" => $user->birthday2,
                    "image1" => 'http://' . $_SERVER['HTTP_HOST'] . '/' . dirname($_SERVER['PHP_SELF']) .$user->image12,
                ),
            );
        }
        
        
        $questions = \ORM::for_table($this->questionTable)
                ->table_alias('question')
                ->select('user.*')
                ->select('question.question')
                ->select('question.id' ,'qid')
                ->inner_join($this->userTable, array('question.user', '=', 'user.id'), 'user')
                ->where('question.game', $game->id)
                ->find_many();
        
        $answerQuestions = array();
        foreach ($questions as $q) {
            $answers = \ORM::for_table($this->answerTable)
                ->table_alias('answer')
                ->select('user.*')
                ->select('answer.answer')
                ->inner_join($this->userTable, array('answer.user', '=', 'user.id'), 'user')   
                ->where('answer.question', $q->qid)
                ->find_many();
            
            $aArr = array();
            
            foreach ($answers as $a) {
                $aArr[] = array(
                    "answer" => $a->answer,
                    "userId" => $a->id,
                    "name" => $a->name,
                    "gender" => $a->gender,
                    "birthday" => $a->birthday,
                    "image1" => 'http://' . $_SERVER['HTTP_HOST'] . '/' . dirname($_SERVER['PHP_SELF']) .$a->image1,
                );
            }
            
            $answerQuestions[] = array(
                "question" => $q->question,
                "userId" => $q->id,
                "name" => $q->name,
                "gender" => $q->gender,
                "birthday" => $q->birthday,
                "image1" => 'http://' . $_SERVER['HTTP_HOST'] . '/' . dirname($_SERVER['PHP_SELF']) .$q->image1,
                "answers" => $aArr
            );
            
        }
        
        
       
        return $this->prepareResult(array('matches'=>$mathes, 'answerQuestions'=>$answerQuestions), 'all');
        
    }
}
