<?php

namespace postServer\method;

class statusCurrentGame extends \postServer\abstraction\method {
    protected $returnFields = array(
        'currentGame'=>'currentGame',   
    );
    
    protected $needFields = array(
        "userId"
    );
    
    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        $game = \ORM::for_table($this->gameTable)
                ->table_alias('game')
                ->join($this->questionTable, array('game.id','=','question.game'),'question')
                ->where_not_in('status', array('closed', 'end'))
                ->where('question.user', $user->id)
                ->find_many();
                
       
        return $this->prepareResult(array('currentGame'=>(bool)count($game)));
        
    }
}
