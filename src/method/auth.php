<?php

namespace postServer\method;

class auth extends \postServer\abstraction\method {

    protected $returnFields = array(
        'id'=>'id_user'
    );
    protected $needFields = array(
        "userId",
        "name",
        "email",
        "gender",
        "birthday"
    );

    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        $user = \ORM::for_table($this->userTable)->where('userId', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $user = \ORM::for_table($this->userTable)->create();
            $user->userId = $_REQUEST['userId'];
        }        
        $user->name = $_REQUEST['name'];
        $user->email = $_REQUEST['email'];
        $user->gender = $_REQUEST['gender'];
        $user->birthday = $_REQUEST['birthday'];
        $user->save();
        
        return $this->prepareResult($user->as_array());
    }

}
