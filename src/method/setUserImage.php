<?php

namespace postServer\method;

class setUserImage extends \postServer\abstraction\method {
    protected $returnFields = array(
        'image1'=>'image1',
        'image2'=>'image2',
        'image3'=>'image3',
        'image4'=>'image4',
        'image5'=>'image5',
        'image6'=>'image6',
    );
    
    protected $needFields = array(
        "userId",
    );
    
    protected $imageKeys = array(
        "image1",
        "image2",
        "image3",
        "image4",
        "image5",
        "image6",
    );


    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        $result = array();

        foreach ($this->imageKeys as $image) {
            
            $res = $this->saveFile($image);
            
            if ($res) {
                $user->$image = $res;
                $result[$image] = $res;
            }
            
            
        }
        $user->save();
        
        return $this->prepareResult($result);
        
    }
}
