<?php

namespace postServer\method;

class setUserProfileById extends \postServer\abstraction\method {
    protected $returnFields = array(
        'message'=>'message'       
    );
    
    protected $needFields = array(
        "userId",
//        "name",
//        "email",
//        "gender",
//        "birthday",
//        "about",
//        "isPushNotification",
    );
    
    protected $imageKeys = array(
        "image1",
        "image2",
        "image3",
        "image4",
        "image5",
        "image6",
    );


    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        $user->name = isset($_REQUEST['name']) ? $_REQUEST['name'] : $user->name ;
        $user->email = isset($_REQUEST['email']) ? $_REQUEST['email'] : $user->email ;
        $user->gender = isset($_REQUEST['gender']) ? $_REQUEST['gender'] : $user->gender ;
        $user->birthday = isset($_REQUEST['birthday']) ? $_REQUEST['birthday'] : $user->birthday ;
        $user->about = isset($_REQUEST['about']) ? $_REQUEST['about'] : $user->about ;
        $user->isPushNotification = isset($_REQUEST['isPushNotification']) ? $_REQUEST['isPushNotification'] : $user->isPushNotification ;
        foreach ($this->imageKeys as $image) {            
            $res = $this->saveFile($image);            
            if ($res) {
                $user->$image = $res;
            }
        }
        $user->save();
        
        return $this->prepareResult(array('message'=>'User updated'));
        
    }
}
