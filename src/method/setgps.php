<?php

namespace postServer\method;

class setgps extends \postServer\abstraction\method {
    protected $returnFields = array(
        'message'=>'message'
    );
    
    protected $needFields = array(
        "userId",
        "latitude",
        "longitude"
    );
    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        $user->latitude = $_REQUEST['latitude'];
        $user->longitude = $_REQUEST['longitude'];
        $user->save();
        
        return $this->prepareResult(array('message'=>'GPS saved'));
        
    }
}
