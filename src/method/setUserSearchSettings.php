<?php

namespace postServer\method;

class setUserSearchSettings extends \postServer\abstraction\method {
    protected $returnFields = array(
        'message'=>'message'
    );
    
    protected $needFields = array(
        "userId",
    );

    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        $user->discover = isset($_REQUEST['discover']) ? $_REQUEST['discover'] : $user->discover ;
        $user->distance = isset($_REQUEST['distance']) ? $_REQUEST['distance'] : $user->distance ;
        $user->ageStart = isset($_REQUEST['ageStart']) ? $_REQUEST['ageStart'] : $user->ageStart ;
        $user->ageEnd = isset($_REQUEST['ageEnd']) ? $_REQUEST['ageEnd'] : $user->ageEnd ;
        $user->gender = isset($_REQUEST['gender']) ? $_REQUEST['gender'] : $user->gender ;
        $user->save();
        
        return $this->prepareResult(array('message'=>'Search settings saved'));
        
    }
}
