<?php

namespace postServer\method;

class removeUser extends \postServer\abstraction\method {
    protected $returnFields = array(
        'message'=>'message'       
    );
    
    protected $needFields = array(
        "userId",
    );
    
    protected $imageKeys = array(
        "image1",
        "image2",
        "image3",
        "image4",
        "image5",
        "image6",
    );


    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();        
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        $user->delete();        
        
        return $this->prepareResult(array('message'=>'User removed'));
        
    }
}
