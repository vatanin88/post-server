<?php

namespace postServer\method;

class removeUserImage extends \postServer\abstraction\method {
    protected $returnFields = array(
        'message'=>'message'       
    );
    
    protected $needFields = array(
        "userId",
        "imageKey"
    );
    



    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        $ik = $_REQUEST['imageKey'];
        
        if (!isset($user->$ik )) {
            $this->error[] = 'imageKey not found';
            return $this->error();
        }
        
        $user->$ik  = '';
        
        $user->save();
        
        return $this->prepareResult(array('message'=>'Image removed'));
        
    }
}
