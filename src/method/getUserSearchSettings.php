<?php

namespace postServer\method;

class getUserSearchSettings extends \postServer\abstraction\method {
    protected $returnFields = array(
        'id'=>'userId',
        'discover'=>'discover',
        'distance'=>'distance',
        'ageStart'=>'ageStart',
        'ageEnd'=>'ageEnd',
        'gender'=>'gender'       
    );
    
    
    protected $needFields = array(
        "userId"
    );
    
    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        return $this->prepareResult($user->as_array());
        
    }
}
