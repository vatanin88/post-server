<?php

namespace postServer\method;

class settoken extends \postServer\abstraction\method {
    protected $returnFields = array(
        'message'=>'message'
    );
    
    protected $needFields = array(
        "userId",
        "token",
        "deviceId"
    );
    
    public function run() {
        $this->checkNeedFields();
        if ($this->error) {
            return $this->error();
        }
        
        $user = \ORM::for_table($this->userTable)->where('id', $_REQUEST['userId'])->find_one();
        if(!$user) {
            $this->error[] = 'User not found';
            return $this->error();
        }
        
        $user->token = $_REQUEST['token'];
        $user->deviceId = $_REQUEST['deviceId'];
        $user->save();
        
        return $this->prepareResult(array('message'=>'Token saved'));
        
    }
}
