<?php

namespace postServer\abstraction;

class method {

    protected $filePath = 'files';
    protected $fileUrl = '/files';
    protected $userTable = 'user';
    protected $gameTable = 'game';
    protected $questionTable = 'question';
    protected $answerTable = 'answer';
    protected $likeTable = 'like';
    protected $returnFields = array();
    protected $needFields = array();
    protected $error = array();

    protected function checkNeedFields() {
        foreach ($this->needFields as $fileld) {
            if (!isset($_REQUEST[$fileld])) {
                $this->error[] = "Empty field " . $fileld;
            }
        }
    }

    protected function error() {
        return array(
            "errorMessage" => implode(", ", $this->error),
            "status" => 0
        );
    }

    protected function prepareResult($data, $return = 'object') {
        $result = array();

        if ($return == 'object') {
            foreach ($this->returnFields as $field => $name) {
                
                if (!isset($data[$field])) {
                    continue;
                }
                
                if (is_array($name)) {
                    
                } else {
                    
                    if (substr($field, 0, 5) == 'image' && $data[$field]) {
                        $result[$name] = 'http://' . $_SERVER['HTTP_HOST'] . '/' . dirname($_SERVER['PHP_SELF']) . $data[$field];
                    } else {
                        $result[$name] = $data[$field];
                    }
                }
            }

            return array(
                'data' => $result,
                'status' => true
            );
        }

        if ($return == 'array') {
            $result['data'] = array();
            $result['status'] = true;
            foreach ($data as $d) {
                $r = array();
                foreach ($this->returnFields as $field => $name) {
                    $r[$name] = $d[$field];
                }
                $result['data'][] = $r;
            }

            return $result;
        }
        
        $result['data'] = $data;
        $result['status'] = true;
        
        return $result;
        
    }

    protected function saveFile($name) {
        if (!isset($_FILES[$name])) {
            return '';
        }
        $uploadPath = dirname(dirname(dirname(__FILE__))) . DIRECTORY_SEPARATOR . $this->filePath . DIRECTORY_SEPARATOR;
        $path_parts = pathinfo(basename($_FILES[$name]['name']));

        
        if (!isset($path_parts['extension'])) {
            $path_parts['extension'] = 'png';
        }

        if (file_exists($uploadPath . $path_parts['filename'] . '.' . $path_parts['extension'])) {
            $tmpName = $path_parts['filename'];
            $i = 0;
            while (file_exists($uploadPath . $tmpName . '.' . $path_parts['extension'])) {
                $tmpName = $path_parts['filename'] . '_' . $i;
                $i++;
            }

            $path_parts['filename'] = $tmpName;
        }

        if (move_uploaded_file($_FILES[$name]['tmp_name'], $uploadPath . $path_parts['filename'] . '.' . $path_parts['extension'])) {
            return $this->fileUrl . '/' . $path_parts['filename'] . '.' . $path_parts['extension'];
        } else {
            return '';
        }
    }

}
