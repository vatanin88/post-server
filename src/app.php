<?php

namespace postServer;


class app {

    private $result = array();

    public function __construct() {

        if (isset($_REQUEST['method'])) {
            $method = "postServer\method\\".$_REQUEST['method'];
        }

        if (class_exists($method)) {
            $method = new $method;
            $this->result = $method->run();
        } else {
            $this->result = array(
                "errorMessage" => "method not found",
                "status" => 0
            );
        }
    }
    

    public function toJson() {
        return json_encode($this->result, JSON_UNESCAPED_UNICODE);
    }

}
