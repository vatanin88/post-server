<?php

require './vendor/autoload.php';

ORM::configure('mysql:host=localhost;dbname=socket_server;charset=utf8');
ORM::configure('username', 'socket_server');
ORM::configure('password', 'server88');

if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
    $locale = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
    setlocale(LC_ALL, $locale);    
}

header('Content-Type: application/json');
$app = new \postServer\app();
echo $app->toJson();